## ABSTRACT

Toddo is todo lists manager with drag'n drop.
* contexts creation 
* items are editable *in situ*

## RÉSUMÉ

Toddo est est un gestionnaire de "todo listes" utilisant le glisser-déposer. Doù le dd, comme dans drag'n drop.
* il est possible de créer, renommer et détruire des contextes
  * chaque contexte contient des zones différentes
* les items sont éditables in situ

![screenshot](toddo.png "Toddo")

## Third Party Acknowledgements - Components 

* dragdroptouch.js (MIT LICENCE)

## About AGPL Licence for Toddo

"Many of the most common free software licenses, such as the original MIT/X license, ... are "GPL-compatible". 
That is, their code can be combined with a program under the GPL without conflict (the new combination would have the GPL applied to the whole)."
see http://en.wikipedia.org/wiki/License_compatibility
       
## INSTALLATION

in /var/www/html 
execute 
>sudo git clone https://gitlab.adullact.net/felie/toddo.git 

* Enter your e-mail adress in config.php 
* Toddo must be able to send emails
* Toddo must be able to write in his *users* directory
sudo chmod 777 users
