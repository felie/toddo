<?php
// configuration pour toddo
$gestion_inscription=true;
$context_document_root=$_SERVER['CONTEXT_DOCUMENT_ROOT'];
$server_name=$_SERVER['SERVER_NAME'];
$request_uri=$_SERVER['REQUEST_URI'];
$path=explode('/',$request_uri);
array_pop($path);
$path=implode('/',$path);
$dirusers="$context_document_root$path/users";
$toddourl="$server_name$path";
$frommail="francois@elie.org";
$fromname="Toddo";
$default_context='INBOX';
$trash_context='TRASH';
$protected_contexts=[$trash_context,$default_context];
$smileys=[
  '?s'=>'😀',
  '?w'=>'⚠',
  '?l'=>'❤️'];
//i18n
$locale='fr_FR.UTF-8';
$domain='Toddo';
putenv("LANG=".$locale); 
putenv('LC_ALL='.$locale);
setlocale(LC_ALL, $locale);
bindtextdomain($domain, 'locale'); 
bind_textdomain_codeset($domain, 'UTF-8');
textdomain($domain);
$zone=[
  _("To do"),
  _("To plan"),
  _("Delegate"),
  _("Maybe someday"),
  _("In progress"),
  _("Done"),
  _("Exchange buffer between contexts"),
  _("Trash can")];

?>
