<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('config.php');
require_once('util.php');
$token='';
if (isset($_GET['k']))
  $token=$_GET['k'];

$userdir="$dirusers/K$token";
if (isset($_GET['context']))
  $context=$_GET['context'];
else
  $context=$default_context;
    
//search engine
if (isset($_GET['chercher'])){
  $k=$_GET['k'];
  $chaine=$_GET['chercher'];
  exec("cd users;grep -rli '$chaine'",$result);
  echo "Recherche de <i>$chaine</i><hr>";
  sort($result,SORT_LOCALE_STRING);
  foreach($result as $context){
    $c=explode('/',$context)[1];
    //echo $context.' ---> '.$c;
    $c=str_replace('-col','',$c);
    $n=$c[strlen($c)-1];
    $c=substr($c,0,strlen($c)-1).' '.$zone[$n-1];
    echo "<a href='?k=$k&context=$c'>$c</a><br>";
    }
  exit;
  }

//renommage du contexte actuel
if (isset($_POST['submitrenamecontext']) and $context!=$default_context)
  {
  $new=$_POST['name'];
  echo $new;
  //voir($contexts);
  echo "cd $userdir;mv '___context-$context' '___context-$new';rename 's/^$context-/$new-/' $context*";
  exec("cd $userdir;mv '___context-$context' '___context-$new';rename 's/^$context-/$new-/' $context*");
  header("Location: index.php?k=$token&context=$new");
  exit;
  }

//add a context
if (isset($_POST['submitaddcontext']) and ($_POST['name']!='')){
  $context=$_POST['name'];
  exec("cd $userdir;touch '___context-$context';ls -l __*",$result);
  header("Location: index.php?k=$token&context=$context");
  exit;
  }
  
// empty a context
if (isset($_POST['submitemptycontext']))
    {
    $todelete=$_POST[name];
    if ($context==$todelete)
        $context=$default_context;
    //if ($todelete!=$default_context)
    array_map('unlink', glob("$userdir/$todelete-col*")); // delete
    file_put_contents("$userdir/___context-$todelete",''); // empty
    header("Location: index.php?k=$token&context=$context");    
    exit;
    }
  
//delete a context
if (isset($_POST['submitdeletecontext']))
    {
    $todelete=$_POST['name'];
    if (!in_array($todelete,$protected_contexts))
        {
        echo "todelete=*$todelete*";
        exec("cd $userdir;rm '___context-$todelete';rm '$todelete-col'*;rm '$todelete-mouchard';rm '$todelete-pmax';ls -l '*$todelete*'",$result);
        }
    if ($todelete==$context)
        $context=$default_context;
    header("Location: index.php?k=$token&context=$context");    
    exit;
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Toddo</title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta name="robots" content="noindex, nofollow">
        <meta name="googlebot" content="noindex, nofollow">
        <meta property="og:image" content="https://<?php echo $toddourl;?>/toddo.jpg">
        <script type="text/javascript" src="dragdroptouch.js"></script>
        <script type="text/javascript" src="jquery-3.3.1.min.js"></script>
    </head>
<body>
<!--<image src="travaux.webp">-->
<style>
<!--<image src="travaux.webp">-->

  /*table, td, tr{
    border:2px solid red;
    }*/
    
  h1{
    display:inline;
  }
    
  .hover:hover{
    background-color:red;
  }
  
  .bctx{
    width:100px;
    margin:0px;
    background-color:lightgray;
  }
  
  .bouton{
    vertical-align:middle;text-decoration:none;display:inline-block;
  }
  .petit{
    width:8%;
  }
  
  .grand{
    width:70%;
  }
  
  form{
  font-size:11px;
  }
  
  #area {
    width:88%;
    height:25vh;
  }
    
  .cercle3{
    background:red;
    margin-left:-10px;
    border-radius:50%;
    width:10px;
    height:10px;
    z-index:2000;
  }
  .cercle2{
    background:orange;
    margin-left:-10px;
    border-radius:50%;
    width:10px;
    height:10px;
    z-index:2000;
  }
  .cercle1{
    background:yellow;
    margin-left:-10px;
    border-radius:50%;
    width:10px;
    height:10px;
    z-index:2000;
  }
  
    .zone table td:first-child::after {
    content: "";
    display: inline-block;
    vertical-align: top;
    min-height: 60px;
  }

  .entete table,table tr,table td {
      padding:0;
      border:0px red solid;
  }
  .arrondi {
          -moz-border-radius: 5px;
          -webkit-border-radius: 5px;
          border-radius: 5px;
  }
  .arrondi2 {
          -moz-border-radius: 5px;
          -webkit-border-radius: 5px;
          border-radius: 5px;
  }
  .context{
      box-shadow: 6px 3px 3px gray;
  }
  
  .item{
      text-align:left; // position des items
      display: block;
      margin-left: 5px;
  }
  .item2 {
      margin:3px;
      margin-right:10px;
    }
    
  .input{
    padding:10px
    }
    
  .scrollable{
      overflow:scroll;
  }
  /*@media all and (orientation:portrait) */
  
@media screen and (max-aspect-ratio: 13/9) { /* portrait */

  #area{
    height:25vh;
    width:68vw;
  }
  
  .chk{
  }
  
  .color0 {background-color:ivory;color:black;font-weight:normal}
  .color1 {background-color:yellow;color:black:font-weight:normal}
  .color2 {background-color:orange;color:black;font-weight:normal}
  .color3 {background-color:red;color:black}
  
  body,textarea,button{
    font-size: 4vmin;
  }
  /* Style adjustments for portrait mode goes here */
  .bl, .pb, .ec, #howto, .ContextManagment, .container1{
    display:none;
  }
  
  .cercle1,.cercle2,.cercle3{
    width:16px;
    height:16px;
    float:right;
    margin-right:-6px;
    border:1px solid black;
  }

    .cercle{
    width:16px;
    height:16px;
    margin-top:-4px;
    display:inline;
    position:relative;
    background:violet;
    margin-right:-14px;
    margin-top:-2px;
    border-radius:50%;
    float:right;
    top:0px;
    left:0vw;
  }
  
  .z {border: 0px solid black;margin:0px;margin-left:5px;}
  /*.bt {float:bottom;}*/
  .t {float:top;}
  .pb {width:70%;height:32%}
  .sc {height:70%;overflow:scroll;overflow-x:hidden;}
  .liste {height:70vh;background-color:ivory;}

  .colonne{
    background-color:lightblue;
  }
  
  .todo{
    width:100%;
    height:70vh;
  }
  
  .item2, .item{
    //background-color:ivory;
    padding:2px;
    margin:2px;
    width:90vw;
  }
  

  #contextesportraits{
    width:15vw;
    background-color:lightblue;
    vertical-align:top;
  }
  .todo{
    vertical-align:top;
  }
  
  .box,lightgreen{
    width:24vw;
    display:block;
    margin:2px;
    padding:0px;
    background-color:blue;
  }
  
  .coin{
    position:absolute;
    top:1.2vh;
    right:2vw;
    width:25vw;
    height:25.7vh;
    background-color:lightblue;
    border:1px solid black;
    overflow-x:hidden;
    overflow-y:auto;
  }
}

@media screen and (min-aspect-ratio: 13/9) { /* landscape */

  #area{
    width:100%;
    }
    
  .coin{
    display:none;
  }
  
  .colonne, .zone{
    width:100%;
    }

      body {background-color:lightblue;padding:3px;margin:0;}

      .bl {clear:left;height:78vh;width:25%;background-color:#ffe6cc}
      .br {clear:right;height:78vh;width:25%;background-color:#ffe6cc}
      .z {display: inline-block;border: 0px solid black;margin:0px;}
      .l {float:left;}
      .r {float:right;}
      .bt {float:bottom;}
      .t {float:top;}
      .pb {width:100%;height:32%}
      /*.sc {height:100%;overflow:scroll;overflow-x:hidden;}*/
      .liste {width:25%;height:78vh;background-color:ivory;}
      .p {width:49%;height:17vh;background-color:lightblue;}
      .color0 {background-color:lightblue;color:black;font-weight:normal;padding:0}
      .color1 {background-color:yellow;color:black:font-weight:normal;padding:0;}
      .color2 {background-color:orange;color:black;font-weight:normal;padding:0}
      .color3 {background-color:red;color:white;font-weight:bold;padding:0}

  *[contenteditable] {
    -webkit-user-select: auto !important; 
  }
  
  .cercle{
    background:violet;
    margin-right:-4px;
    margin-top:-20px;
    border-radius:50%;
    float:right;
    width:8px;
    height:8px;
    z-index:2000;
  }

  table {
  margin:auto;
  border-collapse:collapse;
  }
  table th,table td{
          border-left:0px solid black;
          border-collapse:collapse;
    text-align:center;
  }
  .zone table td {
    /*width:3O0px;*/
    vertical-align:top;
    padding-bottom:200px; /* Indispensable pour le drop à la fin d'une zone. Pourquoi ?*/ 
    height:auto;
  }

  table span {
    width:calc((100vw - 200px) / 4);
    display:block;
    background-color: lightblue;
    margin:3px auto 3px auto;	
    padding:3px;
    border:0px solid black;
    line-height:25px;
    text-align:left;
  }
  .contextes{
      margin-left:10px;
      margin-right:0px;
      margin-top:0px;
      /*height:10vh;*/
      width:100%;
  }
  .bgtext {
              position: relative;
              vertical-align:top;
          }
    
          .bgtext:after {
              margin: 3rem;
              content: "Contextes";
              position: absolute;
              transform: rotate(300deg);
              -webkit-transform: rotate(310deg);
              color: gray;
              right:100px;
              top: 0;
              z-index: -1;
        
          }
  .bgtext1 {
              position: relative;
          }
    
          .bgtext1:after {
              margin: 3rem;
              content: "Tâches";
              position: absolute;
              transform: rotate(300deg);
              -webkit-transform: rotate(310deg);
              color: gray;
              right:100px;
              top: 0;
              z-index: 20000;
          }
  /* grid.css */
  .box { 
          position:relative;
          //writing-mode:horizontal-td;
          height:18px;
          min-width:50px;
          /*background:#EAF8A3;*/
          padding:2px;
          margin:5px;
          //display:grid;
          //background:lightgreen;
          place-items: center;
          text-align:center;
  }

  .box>* {
          flex: 1 1 100px;

      }
      
  .container1 {
      /*width:100%;*/
      display:flex;
      flex-direction:row;
      flex-flow:row wrap;
      flex-wrap:wrap;
      justify-content: left;
      //height:30vh;
      overflow-x:hidden;
      overflow-y:scroll;
  }
  .container2 {
    display:none;
    }
  
  .link{
      text-decoration:none;
      font-weight:bold;
  }
  #update {
      background: url(https://philo-labo.fr/toddo2/update.png) no-repeat;
      background-color:none;
      position:relative;
      border-radius:50%;
      height:50px;
      width:50px;
      right:-20vh;
      margin-top:50px;
  }
}
</style>
<?php

if (!file_exists("$dirusers/K$token"))   // si le token n'existe pas c'est que c'est une inscription
    {
    echo "<h1>Toddo</h1>À quelle adresse dois-je vous adresser un lien secret et permanent vers votre toddo ?<br/><form method='POST' action='mail2user.php'><input style='border:1px dotted gray;' name='mail' type='text' size='40'> <input type='submit' style='-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px;border:1px dotted gray;' value='OK'></form>";
    exit();
    }
    
function voir($s){  
    echo "<pre>";
    print_r($s);
    echo "</pre>";
    }
// entrée normale

 // traitement du formulaire d'entrée des tâches
if (isset($_POST['tasks']))
  $todo=format_tasks($_POST['tasks']);
  
$contexts=glob("$userdir/___context*");
foreach ($contexts as &$c)
  {
  $c=str_replace("$userdir/___context-",'',$c);
  }
if (!in_array($default_context,$contexts))
    $contexts[]=$default_context; // au cas où le répertoire a été effacé
  
if (!in_array($context,$contexts))
    $contexts[].=$context; // si le context actuel est nouveau
unset($contexts[array_search('INBOX', $contexts)]);
unset($contexts[array_search('TRASH', $contexts)]);
sort($contexts,SORT_LOCALE_STRING);
array_unshift($contexts, 'INBOX');
array_push($contexts,'TRASH');



// chargement des données du context dans toutes les zones
for ($i=1;$i<7;$i++) // on ne charge pas la poubelle
  if (file_exists("$userdir/$context-col$i"))
    $col[$i]=format_tasks(file_get_contents("$userdir/$context-col$i"));
  else
    $col[$i]='';
	
$col[7]=format_tasks(file_get_contents("$userdir/tampon")); // le tampon d'échange
$n=0;


function format_tasks($s)
  {
  global $n;
  $result='';
  $s=gere_check($s);
  $s=explode("\n",$s); // transforme en un tableau
  foreach ($s as $t)
    {
    $t=trim(rawurldecode($t));
    if ($t!='')
      {
      $n++;
      $priority=substr_count(substr($t,0,3),'!');
      $aff=preg_replace('~([^"]*)(http[^\s]*)(\s*)~','\1<a class="link" onmousedown="window.open(this.href,\'_blank\')"  contenteditable="false" href="\2" target="_blank">\2</a>\3',$t);
      $aff=preg_replace(['/\*\*([^\*]*)\*\*/','/\*([^\*]*)\*/'],['<b>\1</b>','<i>\1</i>'],$aff);
      $result.="<div class='item2 arrondi color$priority event' id='id$n' draggable='true'><input class='input' id='id$n-content' type='hidden' value=\"$t\"><span class='item color$priority' contenteditable='true'>$aff</span><span class='cercle' onclick='buffer($n)'></span></div>";
      }
    }
  return $result;
  }
  
function zone($n,$liste='')
    {
    global $col,$todo,$zone;
    if ($n==8)
      $col[$n]=''; // la corbeille
    $name=$zone[$n-1];
    if ($n==1)
        $todolist=$todo;
    else 
      $todolist='';
    $h="23vh";
    if ($n==1 or $n==5)
        $h="75vh";
    return "<div class='arrondi ContextManagment' style='background-color:gray;color:white;'>
        <center><b>$name</b></center>
    </div>
     <div class='zone scrollable' id='style$n' style='margin-left:0px;margin-right:0px;margin-top:0px;height:$h;'>
    <table class='colonne'><tr><td class='todo'><input type='hidden' name='col' value='$n'>$todolist $col[$n]</td><td id='contextesportraits'>$liste</td></tr></table>
    </div>";
    }
?>
    <script type="text/javascript">
    //<![CDATA[
    
function playAudio(url) {
  new Audio(url).play();
}

        $(function(){
          $(document).ready(function(){
                $('.item').on("blur", function(event) { // mise à jour quand un contenteditable perd le focus
                    cetid=$(this).parent().attr('id');
                    document.getElementById(cetid).firstElementChild.value=$(this).html();
                    //alert('coucou');
                    $('#kanban').serializeAny();
                });
		$('.event').on("dragstart", function (event) {
			  var dt = event.originalEvent.dataTransfer;
			  dt.setData('Text', $(this).attr('id'));
			});
        $('.arrondi').on("dragenter dragover drop", function (event) {	
		   event.preventDefault();
		   if (event.type === 'drop') {
                        var data = event.originalEvent.dataTransfer.getData('Text',$(this).attr('id'));
			if (data!=$(this).attr('id'))  
			    {
                            de=$('#'+data).detach();
                            de.insertBefore($(this));	
                            $('#kanban').serializeAny();
                            }
			 event.stopPropagation();
		   };
              });
            //$('table td').on("dragenter dragover drop", function (event) {
        $('table td').on("dragenter dragover drop", function (event) {
		   event.preventDefault();
		   if (event.type === 'drop') {
			  var data = event.originalEvent.dataTransfer.getData('Text',$(this).attr('id'));
			  de=$('#'+data).detach();
			  de.appendTo($(this));
			  $('#kanban').serializeAny();
			  playAudio('clic.mp3');
		   }
	   });
	});
        });
        
function toggle(obj){
  if (obj.innerHTML=='□')
    obj.innerHTML='☑';
  else
    obj.innerHTML='□';
    
}

function paste(event){ // paste the content of area after translation of . - +
  event.preventDefault();
  //alert(encodeURIComponent(document.getElementById('area').value));
   $.ajax({
     type: "GET",
     url: 'util.php?replace='+encodeURIComponent(document.getElementById('area').value),
     //data: "id=" + task, // appears as $_GET['id'] @ your backend side
     success: function(data) {
           // data is ur summary
          //alert(data);
          task.innerHTML=data;
          hiddenvalue.value=data;
     }
   });
   $('#kanban').serializeAny();
   document.getElementById('area').value='';
}

function buffer(n){
  obj=document.getElementById('id'+n);
  hiddenvalue=document.getElementById('id'+n+'-content');
  numero=n;
  contenu=obj.children[1].innerText;
  /*
  remplacer les coches par des +
  virer les ^ pour édition ?
  */
  deux=String.fromCharCode(2);
  re = new RegExp(deux, "gm");
  contenu=contenu.replace(re,'');
  contenu=contenu.replace(/□/gm,'-');
  contenu=contenu.replace(/☑/gm,'+');
  contenu=contenu.replace(/•/gm,'.');
  contenu=contenu.replace(' ',' ');
  navigator.clipboard.writeText(contenu);
  document.getElementById('area').innerHTML=contenu;
  task=obj.children[1];
  console.log(contenu);
}
        
function send(a) {
    $.ajax({
        url: "store.php<?php echo "?k=$token&context=$context"; ?>",
        type: "POST",
        data: {
            ssd: "yes",
            data: a,
        },
        dataType: "text",
    });
}

function sendone(context,element) {
    playAudio('clic.mp3');
    $.ajax({
        url: 'storeone.php<?php echo "?k=$token";?>&context='+context,
        type: "POST",
        data: {
            ssd: "yes",
            data: element,
        },
        dataType: "text",
    });
}

function search(){
  event.preventDefault();
  tosearch=document.getElementById('area').value;
  window.location.href='<?php echo "?k=$token";?>&chercher='+tosearch;
  }

function delbyid(id){ // for dropping to a document not present in the DOM!
    document.getElementById(id).remove();
}

function trash(event){
        data = event.dataTransfer.getData('text');
        //value = document.getElementById(data+'-content').value;
        el = document.getElementById(data);
        el.parentNode.removeChild(el); // delete
}

(function($){
    $.fn.serializeAny = function() {
    //alert('serializeAny');
    var ret = [];
     $.each( $(this).find(':input'), function() { // :input
         ret.push(encodeURIComponent(this.name) + "=" + encodeURIComponent($(this).val())); 
         //alert(encodeURIComponent(this.name) + "=" + encodeURIComponent($(this).val()));
    });
    send(ret);
}
})(jQuery);
    	//]]>

  	</script>
  	
<?php  	
    $container2='';
    foreach ($contexts as $ctxt) {
        $color='ivory';
        $ondrop="ondrop=\"
        data = event.dataTransfer.getData('text');
        value = document.getElementById(data+'-content').value;
        sendone('$ctxt',value); 
        delbyid(data);delbyid(data+'-content');\" class='z arrondi' ";
        if ($context==$ctxt) 
            {$color='lightgreen';$ondrop='';$class='box context arrondi2';} // no drop on itself
        else
            {$class='box';}
        $container2.="<a id='ctxt' class='$class' $ondrop style='background-color:$color;text-decoration:none;' href='?k=$token&context=$ctxt'>&nbsp;$ctxt&nbsp;</a>";
        if (file_exists("$userdir/$ctxt-pmax"))
          $pmax=file_get_contents("$userdir/$ctxt-pmax");
        else 
          $pmax=0;
        if ($pmax!='' and $pmax>0)
          $container2.= "<div class='cercle$pmax'></div> ";
        }
?>
  	
        <table width=100%>
         <tr>
         <td class='ContextManagment' style="width:12vw;">
         <button id='howto' class='btcx' style='margin-bottom:5px'><a style="text-decoration:none" target="_blank" href="howto.pdf"><?php echo _("Howto");?></a></button>
         <form method="POST" action='?k=<?php echo $token;?>&context=<?php echo $context;?>'>
           <input id='name' class='bctx' placeholder="contexte" type='text' name='name'>
          <!--</td>
          <td class='ContextManagment' style="width:100px;height:36px">-->
           <input onclick="if (document.getElementById('name').value==''){message='<?php echo _("Empty value");?>';alert(message);return false;}" class='bctx' type='submit' name="submitaddcontext" value='<?php echo _('Create');?>'><br/>
           <input onclick="if (document.getElementById('name').value==''){message='<?php echo _("Empty value");?>';alert(message);return false;}" class='bctx' type='submit' name="submitrenamecontext" value='<?php echo _('Rename');?>'><br/>
           <input onclick="if (document.getElementById('name').value==''){message='<?php echo _("Empty value");?>';alert(message);return false;};message='<?php echo _('You really want to empty this context:');?>';if (!confirm(message+' '+document.getElementById('name').value+' ?')) return false;" class='bctx' type='submit' name="submitemptycontext" value='<?php echo _('Empty');?>'>
           <input onclick="if (document.getElementById('name').value==''){message='<?php echo _("Empty value");?>';alert(message);return false;};message='<?php echo _('You really want to delete this context:');?>';if (!confirm(message+' '+document.getElementById('name').value+' ?')) return false;" class='bctx' type='submit' name="submitdeletecontext" value='<?php echo _('Delete');?>'>
          </td>
          </form>
          <td rowspan="2" style="height:50px;border:0px black solid">
    <!-- entrée des items -->
    <div class='coin'><?php echo $container2;?></div>
    <form style="display:inline;" method="POST" action='?k=<?php echo $token;?>&context=<?php echo $context;?>'>
         <textarea id="area" style="background-color:ivory;" name="tasks"></textarea>
         <button class='bouton grand' type="submit"><h4 style="display:inline"><?php echo _('Add these tasks');?></h4></button>
     </form>
        <button class='bouton petit' onclick="paste(event);"><h4 style="display:inline">&#8595;</h4></button>
        <button class='bouton petit' ondrop="trash(event)"><img width=12 src='images/trash-icon.png'/ ></button>
        <button class='bouton petit'  onclick="search();"><h4 style="display:inline">&#x1F50D;</h4></button>
          </td>
          <td class='ContextManagment' style="padding:5px;text-align:left" rowspan="2" width=50%>
          <div class='bgtext'></div>
          <!--<div class='scrollable' style="border:0px black solid;height:20vh"></div>-->
          <!-- liste des contextes -->
<div class='container1'> <!-- container with box in it - disposition in flex -->
<?php 
    foreach ($contexts as $ctxt) {
        $color='ivory';
        $ondrop="ondrop=\"
        data = event.dataTransfer.getData('text');
        value = document.getElementById(data+'-content').value;
        sendone('$ctxt',value); 
        delbyid(data);delbyid(data+'-content');\" class='z arrondi' ";
        if ($context==$ctxt) 
            {$color='lightgreen';$ondrop='';$class='box context arrondi2';} // no drop on itself
        else
            {$class='box';}
        echo "<a id='ctxt' class='$class' $ondrop style='background-color:$color;text-decoration:none;' href='?k=$token&context=$ctxt'>&nbsp;$ctxt&nbsp;</a>";
        if (file_exists("$userdir/$ctxt-pmax"))
          $pmax=file_get_contents("$userdir/$ctxt-pmax");
        else 
          $pmax=0;
        if ($pmax!='' and $pmax>0)
          echo "<div class='cercle$pmax'></div> ";
        }
        
?>
</div>
    </div>
        </td>
         </tr>
        </table>  
        
  <div id="kanban">
    <div class="r bl z">
      <div class="pb z"><?php echo zone(6);?></div>
      <div class="pb z"><?php echo zone(7);?></div>
      <div class="pb z" style="background-position-x: center;background-size:50%;background-image:url('poubelle.png');background-repeat:no-repeat;"><?php echo zone(8);?></div>
    </div>
    <div class="liste r z sc ec" style='background-color:#e0e0d1'><?php echo zone(5);?></div>
    <div class="l bl z">
      <div class="pb z"><?php echo zone(2);?></div>
      <div class="pb z"> <?php echo zone(3);?></div>
      <div class="pb z"><?php echo zone(4);?></div>
    </div>
    <div class="liste t r z sc todo"><?php echo zone(1);?>
    </div>
  </div>
<script>
$('#kanban').serializeAny();
$(window).on('load', function(){
liste=document.getElementsByClassName('item');
for (var i = 0; i < liste.length; i++) {
    liste[i].addEventListener('input', function() 
    	{if (this.innerHTML.substring(0,3)=='!!!') {this.className='color3';this.parentNode.classList.add('color3');
    	}
    	else if (this.innerHTML.substring(0,2)=='!!') {this.className='color2';this.parentNode.classList.add('color2');}
    	else if (this.innerHTML.substring(0,1)=='!') {this.className='color1';this.parentNode.classList.add('color1');}
    	else {this.className='color0';this.parentNode.classList.add('color0')};
    	},true)
     };
});
// disable F5 for this page!
// slight update to account for browsers not supporting e.which
function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
$(document).on("keydown", disableF5);
</script>
<?php
?>
</body>
</html>
