��          �      <      �     �     �     �     �     �     �     �      �               "     0     7     =  	   E  '   O  &   w  B  �     �     �     �  	                    !   )     K     R     [     n     w  	   �  	   �  +   �  '   �                   
                                                                              	    Add these tasks Create Delegate Delete Done Empty Empty value Exchange buffer between contexts Howto In progress Maybe someday Rename To do To plan Trash can You really want to delete this context: You really want to empty this context: Project-Id-Version: Toddo
PO-Revision-Date: 2023-05-01 16:26+0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: .
 Ajouter ces tâches Créer Faire faire Supprimer Fini Vider Valeur vide Tampon d'échange entre contextes Manuel En cours Un jour peut-être Renommer À faire Planifier Corbeille Vous voulez vraiment détruire ce contexte: Vous voulez vraiment vider ce contexte: 