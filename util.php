<?php

function see_priorities($x){ //$x is an array of tasks
    global $userdir,$context;
    $prio=false;
    $r='';
    foreach ($x as $e){
      $r.="$e \n";
      if (substr($e,0,3)=='!!!')
        $prio=true;
    }
    if ($prio)
      file_put_contents("$userdir/$context-prio",'');
    else
      unlink("$userdir/$context-prio");
    file_put_contents("$userdir/$context-mouchard",serialize($x));
}

function separate_priorities($c)
    {
    global $pmax;
    $p1=[];$p2=[];$p3=[];$n=[];
    for ($i=0;$i<sizeof($c);$i++)
        {
        if (substr($c[$i],0,3)=='!!!')
            {
            $p1[]=$c[$i];
            $pmax=3;
            }
        else
            if (substr($c[$i],0,2)=='!!')
                {
                $p2[]=$c[$i];
                $pmax=max($pmax,2);
                }
            else
                if (substr($c[$i],0,1)=='!')
                    {
                    $p3[]=$c[$i];
                    $pmax=max(1,$pmax);
                    }
                else    
                    $n[]=$c[$i];
        }
    $r=array_merge($p1,$p2,$p3,$n);
    return $r;
    }
    
function replace_first_spaces_by_nbsp($s){
  $r='';
  $i=0;
  $s=htmlentities($s);
  $s=str_replace('&nbsp;',' ',$s);
  while ($i<strlen($s) and $s[$i]==' '){
    $r.='&nbsp;';
    $i++;
    }
  if ($i>0){
    switch ($s[$i]){
      case '.':$add='• ';break;
      case '-':$add='□ ';break;
      case '+':$add='☑ ';break;
      case ' ':$add=' ';break;
      default:$add=$s[$i];
      }
    $r.=$add.substr($s,$i+1,strlen($s)-$i-1);
    }
  else
    $r=$s;
  return $r;
}
    
function gere_check($s){   
  global $smileys;
  $s=explode("\n",$s); // transforme en un tableau
  foreach ($s as &$t)
    $t=replace_first_spaces_by_nbsp($t);
  $s=implode("\n",$s);
  $s=str_replace(chr(13),"",$s); // p* de javascript qui code les LF en CRLF!
  $s=preg_replace("/\n&nbsp;/",chr(2),$s);
  $s=str_replace(chr(2),chr(2)."<br>&nbsp;",$s);
  $s=str_replace('□','<a onclick=\'toggle(this);\'>□</a>',$s);
  $s=str_replace('☑','<a onclick=\'toggle(this);\'>☑</a>',$s);
  foreach ($smileys as $code=>$smiley)
    $s=str_replace($code,$smiley,$s);
  return $s;
  }
  
if (isset($_GET['replace'])){
  $s=rawurldecode($_GET['replace']);
  $s=gere_check($s);
  $s=str_replace("\n",'<br>',$s);
  echo $s; // call by ajax! too lazy to code this in jacascript...
  }
?>
